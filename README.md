Owncloud on CentOS
==================

This docker image provides an owncloud server based on CentOS.

License
=======

Everything published in this repository is GNU GPL 3.
